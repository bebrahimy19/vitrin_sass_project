import React from "react";
import Image from "next/image";

export default function Product() {
  return (
    <div className="container">
      <div className="product-image">
        <Image
          width={50}
          height={20}
          src="http://co0kie.github.io/codepen/nike-product-page/nikeLogo.png"
          alt="nike-logo"
        />
        <div className="product-pic">
          <Image
            width={450}
            height={380}
            src="http://co0kie.github.io/codepen/nike-product-page/nikeShoe.png"
            alt="nike-shoes"
          />
        </div>
        <div className="dots">
          <a href="#!" className="active">
            <i>1</i>
          </a>
          <a href="#!">
            <i>2</i>
          </a>
          <a href="#!">
            <i>3</i>
          </a>
          <a href="#!">
            <i>4</i>
          </a>
        </div>
      </div>

      <div className="product-details">
        <header>
          <h1 className="title">Nike Roshe Run</h1>
          <span className="colorCat">mint green</span>
          <div className="price">
            <span className="before">$150</span>
            <span className="current">$144.99</span>
          </div>
          <div className="rate">
            <a href="#!" className="active">
              ★
            </a>
            <a href="#!" className="active">
              ★
            </a>
            <a href="#!" className="active">
              ★
            </a>
            <a href="#!">★</a>
            <a href="#!">★</a>
          </div>
        </header>
        <article>
          <h5>Description</h5>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </p>
        </article>
        <div className="controls">
          <div className="color">
            <h5>color</h5>
            <ul>
              <li>
                <a href="#!" className="colors color-bdot1 active"></a>
              </li>
              <li>
                <a href="#!" className="colors color-bdot2"></a>
              </li>
              <li>
                <a href="#!" className="colors color-bdot3"></a>
              </li>
              <li>
                <a href="#!" className="colors color-bdot4"></a>
              </li>
              <li>
                <a href="#!" className="colors color-bdot5"></a>
              </li>
            </ul>
          </div>
          <div className="size">
            <h5>size</h5>
            <a href="#!" className="option">
              (UK 8)
            </a>
          </div>
          <div className="qty">
            <h5>qty</h5>
            <a href="#!" className="option">
              (1)
            </a>
          </div>
        </div>
        <div className="footer">
          <button type="button">
            <Image
              height={20}
              width={22}
              alt="cart"
              src="http://co0kie.github.io/codepen/nike-product-page/cart.png"
            />
            <span>add to cart</span>
          </button>
          <a href="#!">
            <Image
              height={20}
              width={22}
              alt="share"
              src="http://co0kie.github.io/codepen/nike-product-page/share.png"
            />
          </a>
        </div>
      </div>
    </div>
  );
}
